package models;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class OrderList {
	List<Order> orders = new ArrayList<Order>() {
		private static final long serialVersionUID = -5699037927005062521L;

		{
			add(new Order("0001", "This is product 1", 1f, 1));
			add(new Order("0002", "This is product 2", 2f, 2));
			add(new Order("0003", "This is product 3", 3f, 3));
			add(new Order("0004", "This is product 4", 4f, 4));
			add(new Order("0005", "This is product 5", 5f, 5));
			add(new Order("0006", "This is product 6", 6f, 6));
			add(new Order("0007", "This is product 7", 7f, 7));
			add(new Order("0008", "This is product 8", 8f, 8));
			add(new Order("0009", "This is product 9", 9f, 9));
			add(new Order("0010", "This is product 10", 10f, 10));
		}};
		
	public OrderList() {}
		
	public List<Order> getOrders() {
		return orders;
	}
	
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
}
